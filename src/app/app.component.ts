import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StarwarsService } from './starwars.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  //declare modles for this component
  users //= [];
  frmId:number = 1
  choices:Object[] = [{cat:'people'},{cat:'species'},{cat:'vehicles'},{cat:'planets'}, {cat:'starships'}]
  whichCategory:String ='people'
  model
  // showFlag:Boolean = false

  
  constructor(private starwarsService:StarwarsService){}


  handleClick(){
    //invoke service passing parameters
    this.starwarsService.getParamData(this.frmId, this.whichCategory)
    .subscribe((result)=>{ this.model = result})//, err => {this.showFlag = false}) tried to set showFlag when an error returned instead of a result
  }

  ngOnInit(){
    //make a call for user data
    this.handleClick()
    
  }

}